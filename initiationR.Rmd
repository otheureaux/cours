---
title: "Initiation à R - Prise en main du logiciel"
output:
  html_document:
    toc: true
    toc_float: true
    number_sections: yes
    theme: flatly
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

***
&nbsp;


# Avant-propos {-}

L'objectif de ce support est de présenter R dans les grandes lignes. Cette intiation vous permettra ensuite de réaliser des analyses statistiques simples (statistiques descriptives et régressions). Plus précisément, vous allez apprendre ici :

* à prendre en main le logiciel R et à manipuler des objets;
* à importer et nettoyer des données externes;

Ce document (rédigé en R !) vous servira donc de support.

R est un logiciel et un langage de programmation dédié aux traitements statistiques, inventé en 1993 et aujourd'hui géré par la *R Development Core Team*. Il est libre (licence GNU).  

La première chose à faire, si ce n'est déjà fait, est d'installer R puis RStudio (un sous-environnement de R particulièrement pratique). Vous pouvez pour cela suivre la procédure décrite sur cette page : https://larmarange.github.io/analyse-R/installation-de-R-et-RStudio.html

Ensuite, il vous faut créer un **projet** et définir un ***working directory***.  


***


# Prise en main du logiciel


## Généralités

R peut tout d'abord servir de **calculatrice**. Par exemple, on peut exécuter des opérations simples comme :

```{r, eval=TRUE}
1+1
```

ou encore :
```{r, eval=TRUE}
(2*4+1)/3
3^2
```

Il existe des **fonctions** (cf. section suivante pour une définition précise) simples pour calculer des opérations simples, comme la racine carrée :
```{r, eval=TRUE}
sqrt(9)
```

ou l'exponentielle, le log, etc. :
```{r, eval=TRUE}
exp(3)
log(10)
log(10, base = 2) # on peut spécifier la base du logarithme
```

La fonction `round()` permet de définir le nombre de décimales en sortie d'une opération, ce qui est pratique :
```{r, eval=TRUE}
round(exp(3), digits = 2) # le 2 correspond au nombre de décimales voulu
```

On peut également créer des séries de valeurs en utilisant **`:`**:
```{r, eval=TRUE}
1:10 # toutes les valeurs entre 1 et 10
```

## Packages et fonctions

R fonctionne avec des **packages**, qui regroupent eux-mêmes des **fonctions**. Il y a un certain nombre de packages installés par défaut (par exemple le package `base`), mais la plupart doit être ajoutée par l'utilisateur selon ses besoins. De nouveaux packages apparaissent ou sont updatés en permanence par la communauté.

Pour installer un package, utilisez la fonction `install.packages()`:
```{r, eval=FALSE}
install.packages("questionr", dependencies = TRUE)
# On ajoute `dependencies = TRUE` pour que les packages dépendants soient aussi installés
```

Il faut ensuite activer le package avec la fonction `library()`:
```{r, eval=TRUE}
library(questionr)
```

Pour visualiser la liste des fonctions inclues dans un package, utilisez le code suivant :
```{r, eval=TRUE}
ls("package:questionr")
```

ou :
```{r, eval=FALSE}
library(help = "questionr")
```

Pour obtenir de l'aide sur une fonction, placez le curseur sur le nom de la nom de la fonction et appuyez sur la touche F1.  
Pour obtenir le code de la fonction, sélectionnez entièrement le nom de la fonction et exécutez.  
Les fonctions contiennent une série d'arguments, qu'il convient de renseigner. Vous trouverez la liste de ces arguments dans l'aide de la fonction, au ainsi :

```{r, eval=TRUE}
args("prop_table")
```


## Les objets dans R

R est un langage orienté objet. On créé systématiquement des objets. Il en existe plusieurs, en fonction de leur nature. Voici les principaux :

* les vecteurs
* les data frames
* les matrices
* les listes

### Les vecteurs

Un vecteur est une série de valeurs ou de caractères, qui doivent être de même nature. Pour créer un vecteur, on utiliser la fonction `c()`:

```{r, eval=TRUE}
vec1 <- c(1,3,2,8,14,22)
vec1
```

ou la fonction `seq()` pour créer une série de valeurs également distribuées :

```{r, eval=TRUE}
vec2 <- seq(1,2,0.2)
vec2
```

Ces deux vecteurs sont numériques : 
```{r, eval=TRUE}
class(vec2)
```

Mais on peut aussi créer des vecteurs de caractères. Par exemple :

```{r, eval=TRUE}
vec3 <- c("Hélène","Claire","Alice","Anissa","Cédric","Céline")
# les guillemets sont indispensables pour les caractères
vec3
```


```{r, eval=TRUE}
class(vec3)
```

Les **facteurs** sont un type particulier de vecteurs. Ce sont des objets très utiles dans R pour stocker les **variables qualitatives**. Ils sont notamment caractérisés par le fait qu'ils contienent des *levels* (nous verrons cela plus tard). Pour convertir un vecteur de caractères en facteur, il faut utiliser la fonction `factor()` :

```{r, eval=TRUE}
vec3 <- factor(vec3)
```


```{r, eval=TRUE}
class(vec3)
```

### Les data frames

Les data frames sont des **tableaux de données**. Les colonnes peuvent être constituées de différents objets (numérique, facteurs, etc.), mais doivent toutes avoir la même longueur. Less data frames sont donc des listes de vecteurs. C'est l'objet que vous utiliserez la plupart du temps.

Créons par exemple un data frame regroupant les trois vecteurs précédents :

```{r, eval=TRUE}
df <- data.frame(vec1,vec2,vec3)
df
```

Les vecteurs deviennent ainsi des variables. On peut d'ailleurs changer leur nom ainsi :
```{r, eval=TRUE}
names(df) <- c("var1","var2","var3")
df
```

Pour appeler une variable d'un data frame, il faut utiliser le signe **`$`** :
```{r, eval=TRUE}
df$var2
```

La logique est la même pour appliquer des fonctions sur ces variables :
```{r, eval=TRUE}
class(df$var2) # nature de la variable var2
mean(df$var2) # moyenne de la variable var2, etc.
```

Certaines fonctions sont utiles pour caractériser la taille des tables (nombre de lignes/colonnes) :
```{r, eval=TRUE}
dim(df) # nombre de lignes et de colonnes, respectivement
ncol(df) # nombre de colonne
nrow(df) # nombre de lignes
```

On peut également créer de nouvelles variables dans le data frame, issues de calculs entre variables existantes :

```{r, eval=TRUE}
df$var4 <- df$var1 + df$var2
df$var4 # var4 est la somme des deux premières variables
```

Quand on explore un grand tableau, il est souvent utile de se servir de la fonction `head()`, qui permet de visualiser les *n* premières lignes de la table :

```{r, eval=TRUE}
head(df, n=3) # fait apparaitre les 3 premières lignes de df
```

Il y a une synthaxe précise pour manipuler les tables, basée sur les crochets `[]`. Par exemple, on peut extraire des éléments, des lignes ou des colonnes de cette manière :

```{r, eval=TRUE}
df[1,] # extraction de la première ligne
df[,1] # extraction de la première colonne
df[1,1] # extraction du premier élément de la table
df[,1:3] # extraction des 3 premières colonnes
df[c(1,4),4] # extraction des lignes 1 et 4 et de la 5e colonne
```

La fonction `subset()` est équivalente :
```{r, eval=TRUE}
subset(df, select = var1:var3) # extraction des 3 premières colonnes
subset(df, var1>5) # extraction des lignes où var1 > 5
subset(df, select = -var2) # extraction de toutes les colonnes sauf la 2ème
```


### Les matrices

Les matrices sont des data frames un peu particulières : ce sont des tableaux dont les colonnes doivent être de même nature. Elles présentent un intérêt sur certains types de traitements, que nous verrons plus tard.

### Les listes

Les listes sont des "méta-objets" qui regroupent des objets de nature hétérogène (un peu comme un fichier `xls` à plusieurs onglets). Par exemple, on peut créer une liste qui va regrouper un vecteur et un data frame :

```{r, eval=TRUE}
list <- list(vec=vec1,df=df)
```

On appelle ensuite un objet de la liste avec le signe **`$`**, comme pour les data frames :
```{r, eval=TRUE}
list$vec
list$df
```

Dans R, les objets résultant de tests statistiques ou de modèles de régression sont des listes, qui intègrent les statistiques du test, les *p*-values, les coefficients de régression, etc., en autant d'objets distincts. On y aura donc affaire régulièrement dans la formation.

&nbsp;


# Import de données externes et *data management*

Vous allez bien entendu avoir à importer vos données externes (par exemple des fichiers excel ou csv) dans R, puis à les nettoyer correctement (vérifier leur qualité, les NAs, la nature des variables, créer de nouvelles variables, etc.) pour pouvoir finalement procéder à vos analyses statistiques.

## Import de données externes

Il existe plusieurs packages dédiés à l'import de fichiers de données externes. Pour importer des fichiers `xls` ou `xlsx`, le package `readxl` et sa fonction `readxl()` feront parfaitement l'affaire. Si votre fichier excel est bien placé dans votre répertoire de travail (correspondant à votre projet), alors seul le nom du fichier est précisé dans le code, sans le chemin en entier. Nous allons ainsi importer le fichier "" :

```{r, eval=TRUE}
library(readxl)
df <- read_excel("data/table_climato.xlsx")
```

Pour ouvrir un fichier `csv`, on utilise de la même façon la fonction `read.csv2()`.

Le premier réflexe après avoir importé une table est de vérifier le type d'objet importé et les premières lignes pour détecter d'éventuels problèmes (inversion de lignes ou de colonnes par exemple) :

```{r, eval=TRUE}
class(df)
head(df, n = 10)
```


## Exploration de la table et nettoyage

On commence par afficher la liste des variables, avant de les étudier plus en détail :

```{r, eval=TRUE}
names(df)
```

### Nature des variables

Dans la sortie de la commande précédente (`head()`), on a une vue générale de la nature de chaque variable. On peut le voir aussi avec l'aide la fonction `str`, qui affiche la structure d'un objet R :

```{r, eval=TRUE}
str(df)
```


### Gestion des données manquantes

Les données manquantes sont très fréquentes. Il faut d'une part connaitre leur nombre, et d'autre part établir de règles sur la façon de les traiter. Pour les compter, la fonction `table()` associée à `is.na` est utile :

```{r, eval=TRUE}
table(is.na(df))
```

Il y a donc une données manquante. Parfois, on peut vouloir changer les NAs en 0 (c'est juste un exemple ici, qu'on n'appliquera pas en réalité) :


```{r, eval=FALSE}
df$jrs_neige[is.na(df$jrs_neige)] <- 0
```

On peut aussi décider de ne garder que les inidividus qui ne contiennent pas de NAs :


```{r, eval=TRUE}
df2 <- df[complete.cases(df),]
nrow(df)
nrow(df2)
```


### Renommer des variables

On veut par exemple changer "temp" par "température" (la 4ème colonne). Une solution possible est :

```{r, eval=FALSE}
names(df)[4] <- "température"
```

### Discrétiser une variable quantitative

Le package `gtools` a une fonction très pratique pour discrétiser une variable :

```{r, eval=TRUE}
library(gtools)
df$amplDisc <- quantcut(df$ampl, 3) # Discrétise la variable en terciles
table(df$amplDisc) # vérification du résultat
```


### Renommer des modalités de facteurs

```{r, eval=TRUE}
levels(df$amplDisc) <- c("faible","moyenne","forte") # on renomme de façon intelligible
table(df$amplDisc) # vérification du résultat
```

&nbsp;

## Création de nouvelles variables


### Opérations entre variables

La concaténation de caractères se fait via la fonction `paste()`, tandis que l'extraction de caractères se fait via `substr()` :

```{r, eval=TRUE}
df$sub <- substr(df$nom,1,3) # extraction des 3 premiers caractères de la variable 'nom'
head(df$sub)
```

Les opérations entre variables ont été vues précédemment.

### Créer des tables de contingence et des aggrégations

Commençons par créer une seconde variable qualitative :

```{r, eval=TRUE}
df$neigeDisc <- quantcut(df$jrs_neige, 4) # Discrétise la variable en quartiles
levels(df$neigeDisc) <- c("faible","moyenne","forte","très forte") # on renomme de façon intelligible

```

La table de contingence croisant jours de neige et amplitude (en version qualitative) se programme ainsi :

```{r, eval=TRUE}
table(df$amplDisc, df$neigeDisc) # Tableau de contigence
```

Si on le veut en proportions :


```{r, eval=TRUE}
prop.table(table(df$amplDisc, df$neigeDisc)) # Tableau de contigence en proportions
```

Et si on veut contrôler le nombre de décimales, on utilise la fonction `round()` :

```{r, eval=TRUE}
round(prop.table(table(df$amplDisc, df$neigeDisc)),2) 
```
&nbsp;

## La manipulation de tables avec `dplyr`

`dplyr` fait partie du *tidyverse*, un univers d'extensions présentant un syntaxe nouvelle et commune facilitant les scripts de tout genre (voir https://juba.github.io/tidyverse/06-tidyverse.html).

Cette syntaxe est structurée autour du pipe `%>%`, qui rend les scripts beaucoup plus instinctifs. En particulier, cette syntaxe est utilisée pour rendre plus lisibles les emboitements de fonctions.

Ainsi, on va passer de 

```{r, eval=FALSE}
round(prop.table(table(df$amplDisc, df$neigeDisc)),2) 
```

à 

```{r, eval=TRUE, message=FALSE}
library(dplyr)
table(df$amplDisc, df$neigeDisc) %>% prop.table() %>% round(2)
```

### Les verbes de `dplyr`

`dplyr` est basé sur des verbes. Voici les principaux que vous aurez à utiliser pour manipuler des tables :

- `filter` et `slice` : pour filter des lignes
- `select` : pour sélectionner des colonnes
- `mutate` : pour créer de nouvelles colonnes
- `rename` : pour renommer des colonnes


Il existe aussi plein de fonctions de requêtes ou de jointures, comme :

- `case_when()` : requête conditionnelle
- `left_joint()`, `right_joint()`, `bind_rows()`, etc. : jointures

Voir la cheat sheet suivante pour un aperçu plus complet : https://raw.githubusercontent.com/rstudio/cheatsheets/main/data-transformation.pdf

### Exemples d'utilisation de `dplyr`


Par exemple, on veut sélectionner les villes où la température moyenne est > 12°C :

```{r, eval=TRUE, message=FALSE}
df %>% filter(temp > 12)
```

Pour sélectionner les villes < 12°C avec une amplitude < 15°C, puis extraire uniquement la colonne "precip" de cette sélection :

```{r, eval=TRUE, message=FALSE}
df %>% filter(temp > 12 & ampl < 15) %>% select(precip)
```

Pour renommer une variable, puis créer une nouvelle variable dérivée d'une autre (ici les pp° en cm au lieu des mm) :

```{r, eval=TRUE}
df %>% rename(tempMoy = temp) %>% mutate(precipCm = precip/10)
```

### Exercices

1. Créer une nouvelle table qui s'appelera 'subDf' comprenant les villes situées à moins de 200 km du littoral dans lesquelles il neige au moins 5 jours par an.
2. Identifiez les villes humides (pp > 700 mm) situées à plus de 300 m d'altitude, et renommer 'alt' en 'altitude', le tout en une ligne de code.
3. Quelle est la moyenne des températures des villes situées à plus de 400 km du littoral ? vous devez répondre en une seule ligne de code, mais avec deux pipes et la fonction `summarise()`.

```{r, echo=FALSE, eval=TRUE}
df %>% filter(dist_litt > 400) %>% summarise(meanTemp = mean(temp))
```
